const baseConfig = require('./webpack.base.config');
const webpack = require('webpack')

baseConfig.mode = 'development';
baseConfig.plugins.push(new webpack.DefinePlugin({
    "_ENV_": JSON.stringify({
        'API_ORIGIN': 'http://localhost:5000',
        'BASE_URL': '/fortfolio/',
    }),
}));

module.exports = baseConfig;
