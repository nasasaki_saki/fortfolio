all:
	mkdir -p app/js
	emcc wasm-src/hello.c -s WASM=1 -o app/js/hello.js -s EXPORTED_RUNTIME_METHODS='["ccall","cwrap"]'
