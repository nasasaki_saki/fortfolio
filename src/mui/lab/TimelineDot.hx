package mui.lab;

import react.ReactComponent.ReactComponentOfProps;

@:jsRequire('@material-ui/lab', 'TimelineDot')
extern class TimelineDot extends ReactComponentOfProps<Dynamic> {
}
