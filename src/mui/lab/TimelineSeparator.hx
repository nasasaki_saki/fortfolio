package mui.lab;

import react.ReactComponent.ReactComponentOfProps;

@:jsRequire('@material-ui/lab', 'TimelineSeparator')
extern class TimelineSeparator extends ReactComponentOfProps<Dynamic> {
}
