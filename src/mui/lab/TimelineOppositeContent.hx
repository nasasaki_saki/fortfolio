package mui.lab;

import react.ReactComponent.ReactComponentOfProps;

@:jsRequire('@material-ui/lab', 'TimelineOppositeContent')
extern class TimelineOppositeContent extends ReactComponentOfProps<Dynamic> {
}
