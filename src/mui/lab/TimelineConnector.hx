package mui.lab;

import react.ReactComponent.ReactComponentOfProps;
import react.types.Record;

@:enum abstract TimelineConnectorClassKey(String) {
    var Root = 'root';
}

typedef TimelineConnectorProps = {
    var ?children:Dynamic;
    var ?classes:Record<TimelineConnectorClassKey>;
}

@:jsRequire('@material-ui/lab', 'TimelineConnector')
extern class TimelineConnector extends ReactComponentOfProps<TimelineConnectorProps> {
    // static inline function styles<TTheme>(theme:TTheme):ClassesDef<TimelineConnectorClassKey>
    //     return TimelineStyles.styles(theme);
}

// @:jsRequire('@material-ui/lab/TimelineConnector/TimelineConnector.js')
// extern class TimelineStyles {
//     static function styles<TTheme>(theme:TTheme):ClassesDef<TimelineConnectorClassKey>;
// }
