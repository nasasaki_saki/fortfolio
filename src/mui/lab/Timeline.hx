package mui.lab;

import react.ReactComponent.ReactComponentOfProps;
import react.types.Record;

@:enum
abstract TimelineAlign(String) {
    var Alternate = 'alternate';
    var Left = 'left';
    var Right = 'right';
}

@:enum
abstract TimelineClassKey(String) {
    var Root = 'root';
    var AlignLeft = 'alignLeft';
    var AlignRight = 'alignRight';
    var AlignAlternate = 'alignAlternate';
}

typedef TimelineProps = {
    var ?align:TimelineAlign;
    var ?children:Dynamic;
    var ?classes:Record<TimelineClassKey>;
}

@:jsRequire('@material-ui/lab', 'Timeline')
extern class Timeline extends ReactComponentOfProps<TimelineProps> {
    // static inline function styles<TTheme>(theme:TTheme):ClassesDef<TimelineClassKey>
    //     return TimelineStyles.styles(theme);
}

// @:jsRequire('@material-ui/lab/Timeline/Timeline.js')
// extern class TimelineStyles {
//     static function styles<TTheme>(theme:TTheme):ClassesDef<TimelineClassKey>;
// }
