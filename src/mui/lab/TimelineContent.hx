package mui.lab;

import react.ReactComponent.ReactComponentOfProps;

@:jsRequire('@material-ui/lab', 'TimelineContent')
extern class TimelineContent extends ReactComponentOfProps<Dynamic> {
}
