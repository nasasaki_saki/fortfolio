package mui.lab;

import react.ReactComponent.ReactComponentOfProps;

@:jsRequire('@material-ui/lab', 'TimelineItem')
extern class TimelineItem extends ReactComponentOfProps<Dynamic> {
}
