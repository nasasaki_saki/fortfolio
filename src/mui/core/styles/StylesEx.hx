package mui.core.styles;

import haxe.extern.EitherType;
import mui.core.styles.Styles;

@:jsRequire('@material-ui/core/styles')
extern class StylesEx {
    // public static inline function makeStyles<TTheme, TClassesDef, T>(styles:EitherType<TClassesDef, TTheme->TClassesDef>, ClassKeys:T,
    //         ?options:StylesOptions<TTheme>):() -> Record<T> {
    //     return _makeStyles(styles, options);
    // }
    // usage: cast to Record<ClassKey> by caller;  TODO: fix
    public static function makeStyles<TTheme, TClassesDef>(styles:EitherType<TClassesDef, TTheme->TClassesDef>, ?options:StylesOptions<TTheme>):() -> Dynamic;
}
