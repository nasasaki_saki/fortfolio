package;

import haxe.extern.Rest;

@:native("Module")
extern class Emscripten {
    public static var onRuntimeInitialized:() -> Void;
    public static function cwrap<T>(ident:String, retType:WrapType, argTypes:Array<WrapType>):(Rest<Dynamic>) -> T;
}

@:enum abstract WrapType(Null<String>) {
    final Number = 'number';
    final String = 'string';
    final Array = 'array';
    final Null = null;
}
