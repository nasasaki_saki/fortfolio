package;

import mui.core.Button;
import mui.core.Container;
import mui.core.TextField;
import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;

typedef BrainFuckTestState = {
    var code:String;
    var result:String;
}

class BrainFuckTest extends ReactComponentOf<Empty, BrainFuckTestState> {
    override public function new(props, context) {
        super(props, context);
        state = {
            code: '{>>>[-]>[-]<<<[->[->+>+<<]>>;[-<<<<+>>>>]<[-<+>]<<]<}{+++++++++}[-]>++++++>+<<;+++++.+.+.',
            result: '',
        }
    }

    var syncResult = '';

    override function render():ReactElement {
        return jsx('
            <$Container maxWidth=${LG} className="contents background-image">
                <$TextField
                    label="BrainFunc"
                    multiline
                    rows=${4}
                    value=${state.code}
                    onChange=${onChange}
                />

                <$Button onClick=${run} > Run </$Button>

                <$TextField
                    label="Result"
                    multiline
                    disabled
                    rows=${4}
                    value=${state.result}
                />
            </$Container>
        ');

    }

    function onChange(ev:Dynamic) {
        setState({code: ev.target.value});
    }

    function run() {
        syncResult = '';
        BrainFCk.run(state.code, brPrint, brInput);
        setState({result: syncResult});
    }

    function brPrint(c:String) {
        syncResult += c;
    }

    function brInput() {
        return 'a';
    }
}
