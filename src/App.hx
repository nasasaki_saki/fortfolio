package;

import components.Portfolio;
import components.VIntro;
import components.common.Header;
import react.ReactComponent;
import react.ReactMacro.jsx;
import react.router.BrowserRouter;
import react.router.Route;
import react.router.Switch;

class App {
    public static function render():ReactElement {
        return jsx('
            <>
                <$Header.render />
                <$BrowserRouter basename=${Env.baseUrl}>
                    <$Switch>
                        <$Route path="/v-intro" component=${VIntro.render} />
                        <$Route path="/br" component=${BrainFuckTest} />
                        <$Route path="/back-test" component=${BackTest} />
                        <$Route path="/font-test" component=${FontTest} />
                        <$Route path="/wasm-test" component=${WasmTest} />
                        <$Route path="/route-test" component=${RouteTest} />
                        <$Route path="/" component=${Portfolio.render} />
                    </$Switch>
                </$BrowserRouter>
            </>
        ');
    }
}
