package tool;

import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;
import tool.ScriptLoader;

typedef WasmLoaderProps = {
    var src:String;
    var ?onLoad:() -> Void;
}

class WasmLoader extends ReactComponentOf<WasmLoaderProps, Empty> {
    override function render():ReactElement {
        return jsx('<$ScriptLoader src=${props.src} type=${Defer} onLoad=${onLoad} />');
    }

    function onLoad(ev) {
        Emscripten.onRuntimeInitialized = onInitialize;
    }

    function onInitialize() {
        props.onLoad();
    }
}
