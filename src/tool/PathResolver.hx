package tool;

class PathResolver {
    public static function PathResolve(path:String) {
        return if (path.charAt(0) == '/') Env.baseUrl + path.substr(1) else path;
    }
}
