package tool;

import js.Browser;
import js.html.Event;
import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;
import uuid.Uuid;

typedef ScriptLoaderProps = {
    var src:String;
    var ?type:ScriptLoadType;
    var ?onLoad:(ev:Event) -> Void;
}

enum ScriptLoadType {
    Default;
    Async;
    Defer;
}

class ScriptLoader extends ReactComponentOf<ScriptLoaderProps, Empty> {
    static final defaultProps = {
        type: Default,
    }

    final id:String;

    override public function new(props, context) {
        super(props, context);
        id = Uuid.nanoId();
    }

    override function render():ReactElement {
        return jsx('
            <div id=${id} />
        ');
    }

    override function componentDidMount() {
        final script = Browser.document.createScriptElement();
        script.src = props.src;
        switch (props.type) {
            case Default:
            case Async:
                script.async = true;
            case Defer:
                script.defer = true;
        }
        if (props.onLoad != null) {
            script.onload = props.onLoad;
        }
        Browser.document.getElementById(id).replaceWith(script);
    }
}
