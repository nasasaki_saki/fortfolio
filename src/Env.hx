package;

@:native("_ENV_")
extern class Env {
    @:native("API_ORIGIN") public static final apiOrigin:String;
    @:native("BASE_URL") public static final baseUrl:String;
}
