package components.common;

import mui.core.CircularProgress;
import mui.core.styles.StylesEx.makeStyles;
import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;
import react.types.Record;

typedef LoadingMaskProps = {
    var ?disabled:Bool;
}

@:enum private abstract LoadingMaskClasskey(String) to String {
    var LoadingMaskBack = "loadingMaskBack";
}

class LoadingMask {
    static final useStyles:() -> Record<LoadingMaskClasskey> = makeStyles({
        loadingMaskBack: {
            position: 'fixed',
            display: 'flex',
            left: '0',
            top: '0',
            backgroundColor: 'rgba(0, 0, 0, 0.25)',
            height: '100vh',
            width: '100vw',
            zIndex: '1000',
            alignItems: 'center',
            justifyContent: 'center',
        }
    });

    public static function render(props:LoadingMaskProps):ReactElement {
        final classes = useStyles();
        return if (props.disabled) jsx('<></>') else jsx('
        <div className=${classes.loadingMaskBack}>
            <$CircularProgress size="10rem" />
        </div>');
    }
}
