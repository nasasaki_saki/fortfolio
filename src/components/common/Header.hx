package components.common;

import js.Browser;
import mui.core.AppBar;
import mui.core.Avatar;
import mui.core.Toolbar;
import mui.core.Typography;
import mui.core.styles.StylesEx;
import react.ReactComponent;
import react.ReactMacro.jsx;
import react.types.Record;
import tool.PathResolver.PathResolve as P;

@:enum private abstract HeaderClassKey(String) to String {
    var AppbarTitle = "appbarTitle";
    var LinkIcon = "linkIcon";
    var AppbarSpacer = "appbarSpacer";
}

class Header {
    static final useStyles:() -> Record<HeaderClassKey> = StylesEx.makeStyles(theme -> ({
        appbarTitle: {
            flexGrow: '1',
        },
        linkIcon: {
            marginLeft: '0.5rem',
        },
        appbarSpacer: theme.mixins.toolbar
    }));

    public static function render():ReactFragment {
        final classes = useStyles();
        return jsx('<>
            <$AppBar>
                <$Toolbar>
                    <$Typography variant=${H6} className=${classes.appbarTitle}>ななさきのページ</$Typography>
                    <$Avatar className=${classes.linkIcon} src=${P("/image/twitter.png")} onClick=${() -> Browser.window.open('https://twitter.com/nasasaki_saki')} />
                    <$Avatar className=${classes.linkIcon} src=${P("/image/nicovideo.png")} onClick=${() -> Browser.window.open('https://www.nicovideo.jp/user/116628485')} />
                    <$Avatar className=${classes.linkIcon} src=${P("/image/showroom.png")} onClick=${() -> Browser.window.open('https://www.showroom-live.com/room/profile?room_id=341359')} />
                    <$Avatar className=${classes.linkIcon} src=${P("/image/marshmallow.png")} onClick=${() -> Browser.window.open('https://marshmallow-qa.com/nasasaki_saki')} />
                </$Toolbar>
            </$AppBar>
            <div className=${classes.appbarSpacer} />
        </>');
    }
}
