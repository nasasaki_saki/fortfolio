package components.common;

import mui.core.Container;
import mui.core.styles.StylesEx.makeStyles;
import react.ReactComponent;
import react.ReactMacro.jsx;
import react.ReactType;
import react.types.Record;

@:enum private abstract WithImageClassKey(String) to String {
    var WithBackgroundImage = "withBackgroundImage";
    var BackgroundImage = "backgroundImage";
}

class WithImageContainer {
    static final useStyles:() -> Record<WithImageClassKey> = makeStyles({
        withBackgroundImage: {
            position: 'relative',
            overflow: 'hidden',
            minHeight: '90vh',
        },
        backgroundImage: {
            position: 'absolute',
            top: '0',
            right: '0',
            width: '100%',
            maxWidth: '35rem',
            height: '100rem',
            zIndex: '-1',
            backgroundImage: 'url("image/keyvisual.png")',
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top center',
        }
    });

    public static function render(props:ContainerProps):ReactElement {
        final classes = useStyles();
        final className = (if (props.className != null) props.className + ' ' else '') + classes.withBackgroundImage;

        return jsx('
            <$Container ${...props} className=${className}>
                ${props.children}
                <div className=${classes.backgroundImage} />
            </$Container>
        ');
    }
}
