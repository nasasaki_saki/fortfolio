package components.common;

import mui.core.Container;
import mui.core.Typography;
import mui.core.icon.SvgIconFontSize;
import mui.core.styles.StylesEx;
import mui.core.typography.TypographyVariant;
import mui.icon.AccountBalanceRounded;
import mui.icon.AssignmentInd;
import mui.icon.AttachmentRounded;
import mui.icon.CakeRounded;
import react.ReactComponent;
import react.ReactMacro.jsx;
import react.types.Record;

@:enum private abstract ProfileClassKey(String) to String {
    var ReadableMask = "readableMask";
}

class Profile {
    static final useStyles:() -> Record<ProfileClassKey> = StylesEx.makeStyles({
        readableMask: {
            backgroundColor: 'rgba(255, 255, 255, 0.5)',
            borderRadius: '1rem',
        }
    });

    public static function render():ReactFragment {
        final classes = useStyles();
        return jsx('
            <$Container maxWidth=${SM} className=${classes.readableMask}>

                <$Typography variant=${H3} align=${Center} color=${TextPrimary}>
                    <$AttachmentRounded fontSize=${Large} />
                    <span>profile</span>
                </$Typography>

                <$Typography variant=${H6}>
                    <$AssignmentInd />
                    <span>Name:</span>
                </$Typography>

                <$Typography variant=${Body1} align=${Center}>
                    <ruby>
                        <rb>七咲</rb>
                        <rp>（</rp>
                        <rt>ななさき</rt>
                        <rp>）</rp>
                    </ruby>

                    <ruby>
                        <rb>紗姫</rb>
                        <rp>（</rp>
                        <rt>さき</rt>
                        <rp>）</rp>
                    </ruby>

                </$Typography>

                <$Typography variant=${H6}>
                    <$CakeRounded />
                    <span>Birthday:</span>
                </$Typography>

                <$Typography variant=${Body1} align=${Center}>
                    2020年 11月 27日
                </$Typography>

                <$Typography variant=${H6}>
                    <$AccountBalanceRounded />
                    <span>所属:</span>
                </$Typography>

                <$Typography variant=${Body1} align=${Center}>
                    バーチャル工業大学 情報工学科
                </$Typography>

            </$Container>
        ');

    }
}
