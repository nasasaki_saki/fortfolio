package components;

import components.common.Profile;
import components.common.WithImageContainer;
import mui.core.styles.StylesEx.makeStyles;
import react.ReactComponent;
import react.ReactMacro.jsx;
import react.types.Record;

@:enum private abstract VIntroClassKey(String) {
    var ContentsSpacing = "contentsSpacing";
}

class VIntro {
    static final useStyles:() -> Record<VIntroClassKey> = makeStyles({
        contentsSpacing: {
            "&> *": {
                marginBottom: '4rem',
            },
            paddingtop: '4rem',
        }
    });

    public static function render():ReactElement {
        return jsx('
            <$WithImageContainer.render maxWidth=${LG} className="contents with-background-image">
                <$Profile.render />
                <div className="background-image" />
            </$WithImageContainer.render>
        ');
    }
}
