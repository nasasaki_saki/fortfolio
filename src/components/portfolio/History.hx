package components.portfolio;

import mui.core.Container;
import mui.core.Typography;
import mui.icon.Cake;
import mui.lab.Timeline;
import mui.lab.TimelineConnector;
import mui.lab.TimelineContent;
import mui.lab.TimelineDot;
import mui.lab.TimelineItem;
import mui.lab.TimelineSeparator;
import react.ReactComponent;
import react.ReactMacro.jsx;

class History {
    public static function render():ReactFragment {
        return jsx('
            <$Container maxWidth=${SM} className="readable">
                <$Typography variant=${H3} align=${Center} color=${TextPrimary}>
                    <$mui.icon.History fontSize=${Large} />
                    <span>timeline</span>
                </$Typography>

                <$Timeline>
                    <$TimelineItem>
                        <$TimelineSeparator>
                            <$TimelineDot>
                                <$Cake />
                            </$TimelineDot>
                            <$TimelineConnector />
                        </$TimelineSeparator>
                        <$TimelineContent>
                            <$Typography variant=${H6}>
                                2020/11/27
                            </$Typography>
                            <$Typography>
                                誕生
                            </$Typography>
                        </$TimelineContent>
                    </$TimelineItem>
                </$Timeline>
            </$Container>
        ');

    }
}
