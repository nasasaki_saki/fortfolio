package components;

import components.common.Profile;
import components.common.WithImageContainer;
import components.portfolio.History;
import components.portfolio.Skills;
import mui.core.styles.StylesEx.makeStyles;
import react.ReactComponent;
import react.ReactMacro.jsx;
import react.types.Record;

@:enum private abstract PortfolioClassKey(String) {
    var ContentsSpacing = "contentsSpacing";
}

class Portfolio {
    static final useStyles:() -> Record<PortfolioClassKey> = makeStyles({
        contentsSpacing: {
            "&> *": {
                marginBottom: '4rem',
            },
            paddingTop: '4rem',
        }
    });

    public static function render():ReactElement {
        final classes = useStyles();
        return jsx('
            <$WithImageContainer.render maxWidth=${LG} className=${classes.contentsSpacing}>
                <$Profile.render />
                <$Skills.render />
                <$History.render />
            </$WithImageContainer.render>
        ');
    }
}
