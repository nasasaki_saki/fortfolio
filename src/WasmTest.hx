package;

import components.common.LoadingMask;
import mui.core.Container;
import mui.core.Input;
import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;
import tool.PathResolver.PathResolve as P;
import tool.WasmLoader;

typedef WasmTestState = {
    var loaded:Bool;
    var a:Int;
    var b:Int;
    var c:Int;
}

class WasmTest extends ReactComponentOf<Empty, WasmTestState> {
    override public function new(props, context) {
        super(props, context);
        state = {
            loaded: false,
            a: 0,
            b: 0,
            c: 0
        }
    }

    override function render():ReactElement {
        final value = if (state.loaded) Emscripten.cwrap('add_3', Number, [Number, Number, Number])(state.a, state.b, state.c) else 0;

        return jsx('<>
            <$LoadingMask.render disabled=${state.loaded} />
            <$Container maxWidth=${LG} className="contents background-image">
                <>WasmTest</>
                <br/>
                <$Input type=${Number} value=${state.a} onChange=${onChangeA} />
                <$Input type=${Number} value=${state.b} onChange=${onChangeB} />
                <$Input type=${Number} value=${state.c} onChange=${onChangeC} />
                <>value = ${Std.string(value)}</>
            </$Container>
            <$WasmLoader src=${P('/js/hello.js')} onLoad=${onWasmLoad} />
        </>');
    }

    function onChangeA(ev:Dynamic) {
        setState({a: Std.parseInt(ev.target.value)});
    }

    function onChangeB(ev:Dynamic) {
        setState({b: Std.parseInt(ev.target.value)});
    }

    function onChangeC(ev:Dynamic) {
        setState({c: Std.parseInt(ev.target.value)});
    }

    function onWasmLoad() {
        setState({loaded: true});
    }
}
