package;

import js.Browser;
import mui.core.Container;
import opencv.OpenCV;
import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;
import tool.PathResolver.PathResolve as P;
import tool.ScriptLoader;

@:jsRequire('opentype.js')
extern class Opentype {
}

@:jsRequire('opentype.js', 'Font')
extern class Font {
    public function new(options:Dynamic):Void;
    public function download():Void;
}

@:jsRequire('opentype.js', 'Glyph')
extern class Glyph {
    public function new(options:Dynamic):Void;
}

@:jsRequire('opentype.js', 'Path')
extern class Path {
    public function new(?options:Dynamic):Void;
    public function moveTo(x:Int, y:Int):Void;
    public function lineTo(x:Int, y:Int):Void;
}

class FontTest extends ReactComponentOf<Empty, Empty> {
    var cv:OpenCV;

    final opentype:Dynamic = js.Lib.require('opentype.js');

    override function render():ReactElement {
        return jsx('<>
            <$Container maxWidth=${LG} className="contents background-image">
                FontTest
            </$Container>
            <$ScriptLoader src=${P('/js/opencv.js')} type=${Defer} onLoad=${onCVLoad} />
        </>');
    }

    override function componentDidMount() {
        final notdefGlyph = new Glyph({
            name: '.notdef',
            unicode: 0,
            advanceWidth: 650,
            path: new Path()
        });

        final aPath = new Path();
        aPath.moveTo(100, 0);
        aPath.lineTo(100, 700);
        aPath.lineTo(100, 200);
        aPath.lineTo(100, 0);
        // more drawing instructions...
        final aGlyph = new Glyph({
            name: 'A',
            unicode: 65,
            advanceWidth: 650,
            path: aPath
        });

        final glyphs = [notdefGlyph, aGlyph];
        final font = new Font({
            familyName: 'OpenTypeSans',
            styleName: 'Medium',
            unitsPerEm: 1000,
            ascender: 800,
            descender: -200,
            glyphs: glyphs
        });
        // font.download();
    }

    function onCVLoad(ev) {
        final window:Dynamic = Browser.window;
        cv = window.cv;
        Browser.console.log(cv);
    }
}
