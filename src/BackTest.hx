package;

import js.Browser;
import mui.core.Container;
import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;

typedef BackTestState = {
    var result:String;
}

class BackTest extends ReactComponentOf<Empty, BackTestState> {
    override public function new(props, context) {
        super(props, context);
        state = {
            result: '',
        }
    }

    override function render():ReactElement {
        return jsx('
            <$Container maxWidth=${LG} className="contents background-image">
                Back test
                ${state.result}
            </$Container>
        ');
    }

    override function componentDidMount() {
        Browser.window.fetch(Env.apiOrigin + '/').then(res -> {
            res.text().then(text -> {
                setState({result: text});
            });
        });
    }
}
