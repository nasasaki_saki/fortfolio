package;

import mui.core.Container;
import react.Empty;
import react.ReactComponent;
import react.ReactMacro.jsx;

class RouteTest extends ReactComponentOf<Empty, Empty> {
    override function render():ReactElement {
        return jsx('
            <$Container maxWidth=${LG} className="contents background-image">
                RouteTest
            </$Container>
        ');
    }
}
