package;

import App;
import js.Browser;
import react.ReactComponent;
import react.ReactDOM;
import react.ReactMacro.jsx;

class Index {
    public static function render():ReactFragment {
        return jsx('<$App.render />');
    }

    static function main() {
        ReactDOM.render(jsx('<Index.render />'), Browser.document.getElementById('app'));
    }
}
