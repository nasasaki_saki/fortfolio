const baseConfig = require('./webpack.base.config');
const webpack = require('webpack')

baseConfig.mode = 'production';
baseConfig.plugins.push(new webpack.DefinePlugin({
    "_ENV_": JSON.stringify({
        'API_ORIGIN': 'https://tranquil-river-39351.herokuapp.com',
        'BASE_URL': '/fortfolio/',
    }),
}));

module.exports = baseConfig;
