SCRIPT_URL=https://docs.opencv.org/4.5.1/opencv.js
SCRIPT_DIR=$(cd $(dirname $0); pwd)
OPENCVJS_DIR=$(dirname $SCRIPT_DIR)/app/js
OPENCVJS_FILE=$OPENCVJS_DIR/opencv.js
mkdir -p $OPENCVJS_DIR

if [ ! -f $OPENCVJS_FILE ]; then
    echo downloading opencv.js
    curl https://docs.opencv.org/4.5.1/opencv.js > $OPENCVJS_FILE
    echo 'done.'
else
    echo 'opencv.js is already exist.'
fi
