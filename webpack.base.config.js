const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const app = path.resolve(__dirname, 'app');
const dist = path.resolve(__dirname, 'dist');

module.exports = {
    entry: app + '/index.js',

    output: {
        path: dist,
        filename: 'index.js',
    },

    devServer: {
        port: 3001,
        publicPath: '/fortfolio/',
        overlay: true,
        serveIndex: true,
        historyApiFallback: {
            rewrites: [
                { from: /./, to: '/fortfolio/index.html' }
            ]
        },
    },

    module: {
        rules: [
            {
                test: /\.png$/,
                use: "file-loader?name=image/[name].[ext]",
            },
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: app + '/index.html',
            filename: 'index.html',
        }),
        new CopyPlugin({
            patterns: [
                {
                    from: "**/*",
                    context: "app",
                    globOptions: { ignore: ["**/css/**", "**/index.html"] },
                },
            ],
        }),
    ],
}
